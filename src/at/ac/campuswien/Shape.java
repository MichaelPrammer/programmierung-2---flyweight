package at.ac.campuswien;

public interface Shape {
    void draw();
}